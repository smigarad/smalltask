![pipeline](https://gitlab.com/smigarad/smalltask/badges/main/pipeline.svg)

## Úkol "Malá práce" do předmětu SSP
* Tento úkol slouží jako názorná úkazka pochopení konceptu kontinuální integrace a generátoru statických stránek.
### Mkdocs - generátor statických stránek
* Jako generátor je zvolen ```mkdocs```. Konfigurace tohoto generátoru se nachází v ```mkdocs.yml```.
* Statické stránky jsou psány v jazyce ```md``` a nachází se ve složce ```docs``` a ```docs/help```.
### Kontinuální integrita
* Konfigurace kontinuální integrity lze najít v ```.gitlab-ci.yml```.
* Momentální konfigurace nainstaluje virtuální prostředí - ```virtualvenv```. Do tohoto přostředí se nainstalují potřebné balíčky a spustí se server pomocí ```mkdocs build```.
### Instalace
* Instalace je velmi jednoduchá a skládá se z následujících kroků.
1. Naklonovat prostředí: ```git clone git@gitlab.com:smigarad/smalltask.git```
2. Nainstalování virtuálního prostředí: ```pip install virtualenv```
3. Vytvoření virtuálního prostředí: ```virtualenv venv```
4. Spuštění virtuálního prostředí: ```source venv/bin/activate```
5. Nainstalování potřebných balíčků: ```pip install -r requirements.txt```
6. Následně stačí spustit server: ```mkdocs build```
### Jak se používá
* Po úspěšné instalaci se rozjede server na localhostu (např: ```http://localhost:3000```).

### Projekt vytvořili
* Radek Šmiga - SMI0132
* Eliška Malcharcziková - MAL0404

